// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.io.IOException;

import edu.wpi.first.apriltag.AprilTagFieldLayout;
import edu.wpi.first.apriltag.AprilTagFields;
import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

public class Apriltag
{
    private AprilTagFieldLayout mLayout;
    private NetworkTable table;
    private NetworkTableEntry tx;
    private NetworkTableEntry ty;
    private NetworkTableEntry ta;
    private NetworkTableEntry tid;
    public double RelX;
    public double RelY;
    public double RelZ;
    public double AbsX;
    public double AbsY;
    public double AbsZ;
    public double Area;
    public int ID;

    public Apriltag()
    {
        try
        {
            mLayout = AprilTagFields.k2023ChargedUp.loadAprilTagLayoutField();
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
        }
        this.table = NetworkTableInstance.getDefault().getTable("limelight");
        this.tx = table.getEntry("tx");
        this.ty = table.getEntry("ty");
        this.ta = table.getEntry("ta");
        this.tid = table.getEntry("tid");
    }

    public double GetRelX()
    {
        if ( this.tx != null)
        {
            return this.tx.getDouble(0.0);
        } else
        {
            return -100000;
        }
    }
    public double GetRelY()
    {
        if ( this.tx != null)
        {
            return this.ty.getDouble(0.0);
        } else
        {
        return -100000;
        }
    }
    public double GetArea()
    {
        if ( this.tx != null)
            return this.ta.getDouble(0.0);
        else
            return -100000;
    }
    public boolean align(frc.robot.Drive drive, double speed)
    {
        if (this.GetRelX() >= 1)
        {
            drive.driveCartesian(0, speed, 0);
            return false;
        }
        else if (this.GetRelX() <= -1)
        {
            drive.driveCartesian(0, -speed, 0);
            return false;
        }
        else
        {
            drive.driveCartesian(0, 0, 0);
            return false;
        }
    }
}   
