// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  //private boolean isSim = true;

  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();

  CANSparkMax mFrontLeftDriveMotor = new CANSparkMax(1, MotorType.kBrushless);
  CANSparkMax mFrontRightDriveMotor = new CANSparkMax(2, MotorType.kBrushless);
  CANSparkMax mRearLeftDriveMotor = new CANSparkMax(3, MotorType.kBrushless);
  CANSparkMax mRearRightDriveMotor = new CANSparkMax(4, MotorType.kBrushless);

  private double autoSpeed;
  private double autoRotate;

  private enum autoStates
  {
    begin,
    hybridScore,
    driveForward,
    done
  }
  private autoStates autosate;

  private frc.robot.Apriltag apriltag;
  private frc.robot.Drive drive;

  private edu.wpi.first.wpilibj.Timer matchTimer = new Timer();
  edu.wpi.first.wpilibj.Timer autoTimer = new Timer();
  private boolean limelight = false;
  //private AprilTagFieldLayout mLayout;

  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);

    //mDrive.setSafetyEnabled(false);     // TO.DO Bad. This is enabled by default for a reason. Find a way to fix.

    autoSpeed = 0.1;     // Speed to move at during auto
    autoRotate = 5;       // Degrees to rotate per rotate command during auto

    apriltag = new Apriltag();
    drive = new Drive(mFrontLeftDriveMotor, mFrontRightDriveMotor, mRearLeftDriveMotor, mRearRightDriveMotor, autoSpeed, autoRotate);
    
    autosate = autoStates.begin;
    //driveForwardCounter = 0;

    matchTimer.start();
  }

  /**
   * This function is called every 20 ms, no matter the mode. Use this for items like diagnostics
   * that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    SmartDashboard.putNumber("Match Counter", matchTimer.get());
    SmartDashboard.putNumber("Auto Counter", autoTimer.get());
    SmartDashboard.putString("Auto State", autosate.toString());

    SmartDashboard.putNumber("LimelightX", apriltag.GetRelX());
    SmartDashboard.putNumber("LimelightY", apriltag.GetRelY());
    SmartDashboard.putNumber("LimelightArea", apriltag.GetArea());
    SmartDashboard.putNumber("AprilTag ID", apriltag.ID);
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select between different
   * autonomous modes using the dashboard. The sendable chooser code works with the Java
   * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
   * uncomment the getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to the switch structure
   * below with additional strings. If using the SendableChooser make sure to add them to the
   * chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    m_autoSelected = m_chooser.getSelected();
    // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    System.out.println("Auto selected: " + m_autoSelected);
    drive.stopMovement();

    autosate = autoStates.begin;
    
    drive.mFrontLeftDriveEncoder.setPosition(0);
    drive.mFrontRightDriveEncoder.setPosition(0);
    drive.mRearLeftDriveEncoder.setPosition(0);
    drive.mRearRightDriveEncoder.setPosition(0);

    autoTimer.reset();
    autoTimer.start();
  }

  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        auto1();
        break;
      case kDefaultAuto:
      default:
        // Put default auto code here
        auto1();
        break;
    }
  }

  public void auto1()
  {
    SmartDashboard.putNumber("AprilTag RelX",apriltag.GetRelX());
    SmartDashboard.putNumber("AprilTag RelY",apriltag.GetRelY());
    SmartDashboard.putNumber("AprilTag Area",apriltag.GetArea());

    switch (autosate)
    {
      //==================
      case begin:
        drive.stopMovement();
        autosate = autoStates.hybridScore;
        break;
      //==================
      case hybridScore:
        // #region code
        // !! Does not work !!
        // BUGBUG Motor does not stop
        // if (limelight = true)
        // {
        //   if (apriltag.GetRelX() <= 1)
        //     drive.driveCartesian(0, autoSpeed, 0);
        //   else if (apriltag.GetRelX() >= -1)
        //   {
        //     drive.driveCartesian(-autoSpeed, 0, 0);
        //   }
        //   else
        //   {
        //     if (apriltag.GetArea() >= 22)
        //     {
        //      drive.driveCartesian(autoSpeed, 0, 0); 
        //     }
        //     else if (apriltag.GetArea() <= 25)
        //     {
        //       drive.driveCartesian(-autoSpeed, 0, 0);
        //     }
        //     else
        //     {
        //       drive.driveCartesian(0, 0, 0);
        //     }
        //     //drive.stopMovement();
        //   }
        //}
        if (limelight = false)
        {
          if (drive.mFrontLeftDriveEncoder.getPosition() >= -2 )
          {
            SmartDashboard.putBoolean("IsMoving", true);
            drive.mDrive.driveCartesian(-autoSpeed,0,0);
          }
          else
          {
            SmartDashboard.putBoolean("IsMoving", false);
            drive.stopMovement();
            autosate = autoStates.driveForward;
          }
        }
        //if (apriltag.GetArea() <= )
        SmartDashboard.putNumber("Encoder Value", drive.mFrontLeftDriveEncoder.getPosition());
        // #endregion
        break;
      //==================
      case driveForward:
        if (drive.mFrontLeftDriveEncoder.getPosition() <= 2 )
        {
          SmartDashboard.putBoolean("IsMoving", true);
          drive.mDrive.driveCartesian(0.1, 0, 0);
        }
        else
        {
          SmartDashboard.putBoolean("IsMoving", false);
          drive.stopMovement();
          autosate = autoStates.done;
        }
        SmartDashboard.putNumber("Encoder Value", drive.mFrontLeftDriveEncoder.getPosition());
        System.out.printf("Encoder Value: %s", drive.mFrontLeftDriveEncoder.getPosition());
        break;
      //==================
      case done:
        drive.stopMovement();
        System.out.printf("Encoder Value: %s", drive.mFrontLeftDriveEncoder.getPosition());
        break;
    }
  }

  
  /** This function is called once when teleop is enabled. */
  @Override
  public void teleopInit() {}

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {}

  /** This function is called once when the robot is disabled. */
  @Override
  public void disabledInit() {}

  /** This function is called periodically when disabled. */
  @Override
  public void disabledPeriodic() {}

  /** This function is called once when test mode is enabled. */
  @Override
  public void testInit() {}

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {
  }

  /** This function is called once when the robot is first started up. */
  @Override
  public void simulationInit() {}

  /** This function is called periodically whilst in simulation. */
  @Override
  public void simulationPeriodic() {}
}
