// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
//import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.drive.MecanumDrive;
//import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;

/** Add your docs here. */
public class Drive {
    public MecanumDrive mDrive;
    private double autoSpeed;
    private double autoRotate;

    private CANSparkMax mFrontLeftDriveMotor;
    private CANSparkMax mFrontRightDriveMotor;
    private CANSparkMax mRearLeftDriveMotor;
    private CANSparkMax mRearRightDriveMotor;

    public RelativeEncoder mFrontLeftDriveEncoder;
    public RelativeEncoder mFrontRightDriveEncoder;
    public RelativeEncoder mRearLeftDriveEncoder;
    public RelativeEncoder mRearRightDriveEncoder;

    public Drive(CANSparkMax FrontLeftDriveMotor,CANSparkMax FrontRightDriveMotor,
        CANSparkMax RearLeftDriveMotor,CANSparkMax RearRightDriveMotor, double autoSpeed, double autoRotate)
    {
      this.mFrontLeftDriveMotor = FrontLeftDriveMotor;
      this.mFrontRightDriveMotor = FrontRightDriveMotor;
      this.mRearLeftDriveMotor = RearLeftDriveMotor;
      this.mRearRightDriveMotor = RearRightDriveMotor;

      mFrontLeftDriveMotor.restoreFactoryDefaults();
      mFrontLeftDriveMotor.setInverted(false);
      mFrontLeftDriveMotor.burnFlash();
      mFrontLeftDriveEncoder = mFrontLeftDriveMotor.getEncoder();
      mFrontLeftDriveEncoder.setPosition(0);
  
      mFrontRightDriveMotor.restoreFactoryDefaults();
      mFrontRightDriveMotor.setInverted(false);
      mFrontRightDriveMotor.burnFlash();
      mFrontRightDriveEncoder = mFrontRightDriveMotor.getEncoder();
      mFrontRightDriveEncoder.setPosition(0);
  
      mRearLeftDriveMotor.restoreFactoryDefaults();
      mRearLeftDriveMotor.setInverted(false);
      mRearLeftDriveMotor.burnFlash();
      mRearLeftDriveEncoder = mRearLeftDriveMotor.getEncoder();
      mRearLeftDriveEncoder.setPosition(0);
  
      mRearRightDriveMotor.restoreFactoryDefaults();
      mRearRightDriveMotor.setInverted(false);
      mRearRightDriveMotor.burnFlash();
      mRearRightDriveEncoder = mRearRightDriveMotor.getEncoder();
      mRearRightDriveEncoder.setPosition(0);

      mDrive = new MecanumDrive(FrontLeftDriveMotor, RearLeftDriveMotor, FrontRightDriveMotor, RearRightDriveMotor);
    }
    public void driveCartesian(double x, double y, double rotate)
    {
      mDrive.driveCartesian(x, y, rotate);
    }
    public void driveForward(){
        mDrive.driveCartesian(this.autoSpeed, 0, 0);
      }
      public void driveBackwards(){
        mDrive.driveCartesian(-autoSpeed, 0, 0);
      }
      public void driveLeft(){
        mDrive.driveCartesian(0, -autoSpeed, 0);
      }
      public void driveRight(){
        mDrive.driveCartesian(0, autoSpeed, 0);
      }
      public void rotateClockwise(){
        mDrive.driveCartesian(0, 0, autoRotate);
      }
      public void rotateCounterclockwise(){
        mDrive.driveCartesian(0, 0, -autoRotate);
      }
      public void stopMovement(){
        mDrive.driveCartesian(0, 0, 0);
      }
}
